﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] float movementPeriod = .5f;
    [SerializeField] ParticleSystem baseBlast;
    void Start()
    {
        PathFinder pathFinder = FindObjectOfType<PathFinder>();
        var path = pathFinder.GetPath();
        StartCoroutine(FollowPath(path));
    }
    IEnumerator FollowPath(List<Waypoint2>path)
    {
        foreach (Waypoint2 waypoint in path)
        {
            transform.position = waypoint.transform.position;
            //print("Visiting: " + waypoint);
            yield return new WaitForSeconds(movementPeriod);
        }
        DestroyBase();
    }

    private void DestroyBase()
    {
        var blast = Instantiate(baseBlast, transform.position, Quaternion.identity);
        blast.Play();

        Destroy(blast.gameObject, blast.main.duration);
        Destroy(gameObject);
    }
}
