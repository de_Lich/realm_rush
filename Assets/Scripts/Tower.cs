﻿using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] Transform objectPan;
    [SerializeField] float attackRange = 30f;
    [SerializeField] ParticleSystem projectile;

    public Waypoint2 baseWaypoint;

    Transform targetEnemy;
    

    void Update()
    {
        SetTargetEnemy();
        if (targetEnemy)
        {
            objectPan.LookAt(targetEnemy);
            FireAtEnemy();
        }
        else
        {
            Shoot(false);
        }
    }

    private void SetTargetEnemy()
    {
        AimEnemy [] sceneEnemies = FindObjectsOfType<AimEnemy>();
        if (sceneEnemies.Length == 0)   { return;  }

        Transform closestEnemy = sceneEnemies[0].transform;

        foreach(AimEnemy testEnemy in sceneEnemies)
        {
            closestEnemy = GetClosestEnemy(closestEnemy, testEnemy.transform);
        }

        targetEnemy = closestEnemy;
    }

    private Transform GetClosestEnemy(Transform transformA, Transform transformB)
    {
        var distToA = Vector3.Distance(transform.position, transformA.position);
        var distToB = Vector3.Distance(transform.position, transformB.position);
        if (distToA < distToB)
        {
            return transformA;
        }
            return transformB;
    }

    private void FireAtEnemy()
    {
        float distanceToEnemy = Vector3.Distance(targetEnemy.transform.position, gameObject.transform.position);
        if (distanceToEnemy <= attackRange)
        {
            Shoot(true);
        }
        else
        {
            Shoot(false);
        }
    }

    private void Shoot(bool isActive)
    {
        var emissionBullets = projectile.emission;
        emissionBullets.enabled = isActive;
    }
}
