﻿using UnityEngine;

public class AimEnemy : MonoBehaviour
{
    [SerializeField] Collider collisionMesh;
    [SerializeField] int hitPoints = 10;
    [SerializeField] ParticleSystem hitDamage;
    [SerializeField] ParticleSystem blastPrefab;
    [SerializeField] AudioClip projectileDamageFX;
    [SerializeField] AudioClip enemyDeathFX;


    AudioSource destroySFX;
    void Start()
    {
        destroySFX = GetComponent<AudioSource>();
    }
    void OnParticleCollision(GameObject other)
    {
        ProccessHit();
        if (hitPoints <= 0)
        {
            KillEnemy();
        }
    }

    private void ProccessHit()
    {
        hitPoints = hitPoints - 1;
        var hit =  Instantiate(hitDamage, transform.position, Quaternion.identity);
        destroySFX.PlayOneShot(projectileDamageFX);

        
        hit.Play();
        Destroy(hit.gameObject, hit.main.duration);
    }

    private void KillEnemy()
    {
        var blast = Instantiate(blastPrefab, transform.position, Quaternion.identity);
        Destroy(blast.gameObject, blast.main.duration);
        AudioSource.PlayClipAtPoint(enemyDeathFX, Camera.main.transform.position);
        Destroy(gameObject);
    }
}
