﻿using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{
    [SerializeField] int towerLimit = 5;
    [SerializeField] Tower tower;
    [SerializeField] Transform TowerParent;

    Queue<Tower> towerQueue = new Queue<Tower>();

    public void AddTower(Waypoint2 baseWaypoint)
    {
        int numTower = towerQueue.Count;
        if (numTower < towerLimit)
        {
            InstantiateNewTower(baseWaypoint);
        }
        else
            MoveExistingTower(baseWaypoint);
    }
    private void InstantiateNewTower(Waypoint2 baseWaypoint)
    {
        var newTower =  Instantiate(tower, baseWaypoint.transform.position, Quaternion.identity);
        newTower.transform.parent = TowerParent;
        baseWaypoint.isPlaceable = false;

        newTower.baseWaypoint = baseWaypoint;
        baseWaypoint.isPlaceable = false;
        towerQueue.Enqueue(newTower);
    }

    private void MoveExistingTower(Waypoint2 newBaseWaypoint)
    {
        var oldTower = towerQueue.Dequeue();
        oldTower.baseWaypoint.isPlaceable = true;
        newBaseWaypoint.isPlaceable = false;

        oldTower.baseWaypoint = newBaseWaypoint;

        oldTower.transform.position = newBaseWaypoint.transform.position;

        towerQueue.Enqueue(oldTower);
    }


}
